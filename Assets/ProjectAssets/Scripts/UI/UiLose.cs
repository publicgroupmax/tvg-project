using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiLose : MonoBehaviour
{ 
#pragma warning disable 0649  

    [SerializeField] private Text _txtScore;
    [SerializeField] private Text _txtBestScore;

#pragma warning restore 0649 

    public void Initialize()
    {
        gameObject.SetActive(true); 
        _txtScore.text = LevelManager.Instance.Score.ToString();
        _txtBestScore.text = LevelManager.Instance.BestScore.ToString();
    }

    public void Retry()
    {
        LevelManager.Instance.Retry();
    }


}
