using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiMenu : MonoBehaviour
{ 
#pragma warning disable 0649  

    [SerializeField] private string _labelBestScore;
    [SerializeField] private Text _txtBestScore;
    [SerializeField] private string _labelGamePlayed;
    [SerializeField] private Text _txtGamePlayed;
    [SerializeField] private Text _txtCountCrystals;
    

#pragma warning restore 0649 

    public void Initialize()
    {
        gameObject.SetActive(true);

        _txtBestScore.text = _labelBestScore + LevelManager.Instance.BestScore;
        _txtGamePlayed.text = _labelGamePlayed + LevelManager.Instance.GamePlayed;
        _txtCountCrystals.text = "Crystals: " + LevelManager.Instance.Crystal.ToString();
    }

    public void StartLevel()
    {
        LevelManager.Instance.LevelStart();
    }

}
