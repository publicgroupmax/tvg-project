using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    
#pragma warning disable 0649
    
    [Header("Panels Linc")]
    [SerializeField] private UiMenu _scMenu;
    [SerializeField] private UiInGame _sclInGame;
    [SerializeField] private UiPause _scPause; 
    [SerializeField] private UiLose _scLose;
    [SerializeField] private UiSetting _scSetting; 
    
    public static CanvasManager Instance;
    
#pragma warning restore 0649
    
    private void Awake()    
    {
        if (Instance == null) Instance = this;
        else Debug.LogError("Double Instance on" + gameObject.name +" & " + Instance.gameObject.name);
    }
    
    private void Start()
    {              
        FalsePanels(); 
        _scMenu.Initialize();
        
        LevelManager.Instance.OnLevelStart += OnLevelStart;
        LevelManager.Instance.OnLevelLost += OnLevelLost;
        LevelManager.Instance.OnPause += OnPause;
    }

    private void OnLevelStart()      
    { 
        FalsePanels(); 
        _sclInGame.Initialize();
    }

    private void OnLevelLost()
    {
        FalsePanels();
        _scLose.Initialize();
    }

    private void OnPause(bool value)
    {
        FalsePanels();
        if (value) _scPause.Initialize();
        else _sclInGame.Initialize();
    }

    public void LevelStart()
    {LevelManager.Instance.LevelStart();}

    private void FalsePanels()
    {
        _scMenu.gameObject.SetActive(false);
        _sclInGame.gameObject.SetActive(false);
        _scPause.gameObject.SetActive(false);
        _scLose.gameObject.SetActive(false);
        _scSetting.gameObject.SetActive(false);
    }


}
