﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wow : MonoBehaviour
{
#pragma warning disable 0649 
    
    private GameObject _curWow;
    [SerializeField] private float _timeWaitWow;
    [SerializeField] private List<GameObject> _listWow; 
    
#pragma warning restore 0649 

    private void Start()
    {
        LevelManager.Instance.OnCristalPickUp += ShoveWow;
    }
    
    private void ShoveWow()
    {
        if (_curWow != null)
        {
            StopCoroutine(nameof(HideWow));
            _curWow.SetActive(false);
        }
        
        _curWow = _listWow[Random.Range(0, _listWow.Count)];
        
        _curWow.SetActive(true);
        StartCoroutine(nameof(HideWow));
    }
                                             
    private IEnumerator HideWow()  
    {
        yield return new WaitForSeconds(_timeWaitWow);
        _curWow.SetActive(false);
        _curWow = null;

    }
  
}
