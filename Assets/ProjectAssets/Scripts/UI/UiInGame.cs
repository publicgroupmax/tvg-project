﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class UiInGame : MonoBehaviour 
{

#pragma warning disable 0649    

    [SerializeField] private Text _score;  
    public int SetScore {set => _score.text = "Score: " + value;}
    [SerializeField] private Text _crystal;
    public int SetCrystal {set => _crystal.text = "Crystal: " + value;}

    private bool _autoControl;
    
#pragma warning restore 0649                                                         

    private void Start()
    {
        LevelManager.Instance.OnTouchTern += OnTouchTern;
        LevelManager.Instance.OnCristalPickUp += OnCristalPickUp;
        LevelManager.Instance.OnAutoControl += OnAutoControl;

    }
    
    public void Initialize()
    {
        gameObject.SetActive(true);
        SetScore = 0;
        SetCrystal = LevelManager.Instance.Crystal;
    }

    private void OnAutoControl(bool value)
    {
        _autoControl = value;
    }

    public void TouchTern()
    {
        if(_autoControl) return;
        
        LevelManager.Instance.TouchTern();
    }

    private void OnTouchTern()
    {
        SetScore = LevelManager.Instance.Score;
    }

    private void OnCristalPickUp()
    {
        SetCrystal = LevelManager.Instance.Crystal;
    }

    public void AutoControl(bool value)
    {
        _autoControl = value;
        LevelManager.Instance.AutoControl(value);
    }

    public void Pause()
    {
        LevelManager.Instance.Pause(true);
    }
   
}
