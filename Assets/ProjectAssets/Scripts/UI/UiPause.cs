using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiPause : MonoBehaviour
{ 
#pragma warning disable 0649   

    [SerializeField] private Toggle _toggleMute;
   

#pragma warning restore 0649 

    public void Initialize()
    {
        _toggleMute.isOn = AudioManager.Instance.MuteAudio;
        gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void Back()
    {
        LevelManager.Instance.Pause(false);
        Time.timeScale = 1;
    }
    
    public void AutoControl(bool value)
    {
        LevelManager.Instance.AutoControl(value);
    }

    public void Mute(bool value)
    {
        AudioManager.Instance.MuteAudio = value;
    }



}
