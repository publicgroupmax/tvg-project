﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

//using GameAnalyticsSDK;

public class LevelManager : MonoBehaviour
{
    public event Action OnLevelStart;
    public event Action OnLevelLost;
    public event Action OnTouchTern;
    public event Action OnCristalPickUp;
    public event Action<bool> OnAutoControl;
    public event Action<bool> OnPause; 

    private int _score;
    public int Score
    {
        get => _score;
        set => _score = value;
    }
    public int Crystal
    {
        get => PlayerPrefs.GetInt("Crystal");
        set => PlayerPrefs.SetInt("Crystal", value);
    }
    public int BestScore
    {
        get => PlayerPrefs.GetInt("BestScore");
        set => PlayerPrefs.SetInt("BestScore", value);
    }
    public int GamePlayed
    {
        get => PlayerPrefs.GetInt("GamePlayed");
        set => PlayerPrefs.SetInt("GamePlayed", value); 
    }

#pragma warning disable 0649 
    
    

#pragma warning restore 0649  

    public static LevelManager Instance;

    private void Awake()
    {                          
        if (Instance == null) Instance = this;
        else Debug.LogError("Double Instance on" + gameObject.name +" & " + Instance.gameObject.name);
    }

    public void LevelStart()
    {
        _score = 0;
        GamePlayed += 1;
        
        Taptic.Success();
       
        OnLevelStart?.Invoke();
                                       
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, LevelLoader.NumLevel);
    }

    public void LevelLost()
    {
        if (BestScore < _score) BestScore = _score;
      
        Taptic.Failure();
        
        OnLevelLost?.Invoke();

        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail,LevelLoader.NumLevel);
    }

    public void TouchTern()
    {
        Score += 1;
        if (Score % 5 == 0) LevelGeneration.Instance.ChangeMaterial();
        OnTouchTern?.Invoke();
    }

    public void CristalPickUp()
    {
        Crystal += 1;
        
        OnCristalPickUp?.Invoke();
    }

    
    public void AutoControl(bool value)
    {
        OnAutoControl?.Invoke(value);
    }

    public void Pause(bool value)
    {
        OnPause?.Invoke(value);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    //ClearProgress
    public void ClearProgress()
    {
        PlayerPrefs.DeleteAll();
    }
}
