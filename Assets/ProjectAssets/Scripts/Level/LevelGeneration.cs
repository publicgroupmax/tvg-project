using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    
#pragma warning disable 0649 

    [SerializeField] private int _countGenerate;
    [SerializeField] private GameObject _segment;
    [SerializeField] private GameObject _cristal;
    [SerializeField][Range(0,100)] private int _cristalSpawnChance;
    
    [SerializeField] private Vector3 _vector;
    [SerializeField] private List<GameObject> _poolSegment;

    [SerializeField] private Transform _borderDown;
    [SerializeField] private Transform _borderRight;
    [SerializeField] private Transform _borderLeft;

    [SerializeField] private GameObject _startPlace;
    private List<GameObject> _allSegment = new List<GameObject>();

    [Space] [Header("Materials")] [SerializeField]
    private List<Material> _listMaterials;
    private int _indexMaterial;

#pragma warning restore 0649  

    private Camera _cam;
    private float _width;
    private float _height;

    public static LevelGeneration Instance;

    private void Awake()
    {                          
        if (Instance == null) Instance = this;
        else Debug.LogError("Double Instance on" + gameObject.name +" & " + Instance.gameObject.name);
    
        GeneratePull();
    }
    
    void Start()
    {
        _cam = Camera.main;
        if (_cam != null) _width = _cam.pixelWidth; //верхний
        if (_cam != null) _height = _cam.pixelHeight; //правый
       
    }

    private void GeneratePull()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject obj = Instantiate(_segment, transform);
            _allSegment.Add(obj);
            
            obj.name = i.ToString();
            _poolSegment.Add(obj);
            InstallSegment();
        }
    }

    // Update is called once per frame
   
    void Update()
    {
        InstallSegment();
        CheckBorders();
    }

    private void InstallSegment()
    {
        if (_poolSegment.Count <= 0) return;

        _vector += Random.Range(0, 100) > 50 ? Vector3.right : Vector3.forward;
       
        _poolSegment[0].transform.rotation = Quaternion.identity;
        _poolSegment[0].transform.position = _vector;
      
        _poolSegment[0].GetComponent<Segment>().Cristal = Random.Range(0,100) < _cristalSpawnChance;
        
        _poolSegment.RemoveAt(0);
    }

    public void ToPool(GameObject segment)
    { 
        _poolSegment.Add(segment);
    }

    public void ChangeMaterial()
    {
        _indexMaterial += 1;
        if (_indexMaterial >= _listMaterials.Count) _indexMaterial = 1;
       
        Debug.Log(_indexMaterial);
       
        for (int i = 0; i < _allSegment.Count; i++)
        {
            _allSegment[i].GetComponent<MeshRenderer>().material = _listMaterials[_indexMaterial];
        }
        if (_startPlace) _startPlace.GetComponent<MeshRenderer>().material = _listMaterials[_indexMaterial];
    }

    private void CheckBorders()
    {
        _borderDown.position = new Vector3(0, _borderDown.localScale.y *.5f, (_cam.ScreenToWorldPoint(new Vector2(0, 0)).z));
                            
        _borderRight.position = new Vector3(_cam.ScreenToWorldPoint(new Vector2(_width, 0)).x, _borderDown.localScale.y * .5f, 0); 
        _borderLeft.position = new Vector3(_cam.ScreenToWorldPoint(new Vector2(0, 0)).x, _borderDown.localScale.y * .5f, 0);
    }
    
}
