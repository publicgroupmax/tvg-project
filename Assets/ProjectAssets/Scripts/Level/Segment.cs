using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
#pragma warning disable 0649

    [SerializeField] private bool _reUse; 
    [SerializeField] private GameObject _cristal;
    public bool Cristal { set => _cristal.SetActive(value); }
    private Rigidbody _rb;
    
#pragma warning restore 0649
    
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void DropSegment()
    {
        _rb.isKinematic = false;
        Invoke(nameof(ToPool),1f);
    }

    private void ToPool()
    {
        if (!_reUse)
        {
            Destroy(gameObject);
            return;
        }
        _rb.isKinematic = true;
        _cristal.SetActive(false);
        transform.rotation = Quaternion.identity;
        GetComponentInParent<LevelGeneration>().ToPool(this.gameObject);
    }
}
