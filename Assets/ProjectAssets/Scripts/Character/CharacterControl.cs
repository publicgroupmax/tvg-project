using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    private enum Direction {Right,Left}
    private Direction _direction = Direction.Right;
     
#pragma warning disable 0649 
         
    [Space][Header("Controls")]
    [SerializeField] private float _speedForward;
    private float _curSpeed = 0;
    
    [SerializeField] private float _deadY;
    
#pragma warning restore 0649  

    private bool _autoControl;

    // components
    private Rigidbody _rb;
    
    public static CharacterControl Instance; 

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Debug.Log("Duplicate script" + Instance.gameObject.name + " && " + gameObject.name);
    }

    void Start()
    {
        LevelManager.Instance.OnLevelStart += LevelStart;
        LevelManager.Instance.OnLevelLost += LevelLost;
        LevelManager.Instance.OnTouchTern += OnTouchTern;
        LevelManager.Instance.OnAutoControl += OnAutoControl;
        
        _rb = GetComponent<Rigidbody>();
    }

    private void LevelStart()
    {
        _curSpeed = _speedForward;
    }

    private void OnTouchTern()
    {
        _direction = _direction == Direction.Left ? Direction.Right : Direction.Left;
    }

    private void LevelLost()
    {
        _curSpeed = 0;
    }

    private void OnAutoControl(bool value)
    {
        _autoControl = value;
    }
   
    void FixedUpdate()
    {
        
        if(_curSpeed <= 0) return;
        if (transform.position.y < _deadY) LevelManager.Instance.LevelLost();
        Move(); 
    }

    private void Move()
    {
        if(_autoControl) RayLook();
        Vector3 dir = _direction == Direction.Left ? Vector3.forward : Vector3.right;
        dir += Vector3.down;

        _rb.velocity = dir * _curSpeed;  
    }

    private void OnTriggerExit(Collider other)
    {
        Segment sc = other.GetComponent<Segment>();
        if (sc) sc.DropSegment();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cristal"))
        {
            other.GetComponentInParent<Segment>().Cristal = false;
            LevelManager.Instance.CristalPickUp(); 
        }
    }
    
    private void RayLook()
    {
        Vector3 rayPos = transform.position + (_direction == Direction.Left ? Vector3.forward : Vector3.right)*.5f;
        Debug.DrawLine(rayPos , rayPos + Vector3.down, Color.blue);
     
        LayerMask mask = LayerMask.GetMask("Floor");
        if (!Physics.Raycast(rayPos,Vector3.down,10f, mask))LevelManager.Instance.TouchTern();
    }

}
