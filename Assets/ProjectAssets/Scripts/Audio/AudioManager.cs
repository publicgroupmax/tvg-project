using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeSource {Window, Touch, Cristal, GameOver }
 
public class AudioManager : MonoBehaviour
{
       
#pragma warning disable 0649

    [SerializeField] private AudioSource _sourceWindow;
    [SerializeField] private AudioSource _sourceTouch;
    [SerializeField] private AudioSource _sourceCrystal;
    [SerializeField] private AudioSource _sourceGameOver;
   
#pragma warning restore 0649
 
    public bool MuteAudio
    {
        get { return PlayerPrefs.GetInt("MuteAudio") == 1; }
        set { PlayerPrefs.SetInt("MuteAudio", value ? 1 : 0); }
    }

    public static AudioManager Instance;

    private void Awake()
    {                          
        if (Instance == null) Instance = this;
        else Debug.LogError("Double Instance on" + gameObject.name +" & " + Instance.gameObject.name);
    }

    private void Start()
    {
        LevelManager.Instance.OnLevelStart += PlayWindow;
        LevelManager.Instance.OnLevelLost += PlayGameOver;
        LevelManager.Instance.OnTouchTern += PlayTouch;
        LevelManager.Instance.OnCristalPickUp += PlayCrystal;
        LevelManager.Instance.OnAutoControl += PlayTouch;
        LevelManager.Instance.OnPause += PlayTouch;
    }

    private void PlayWindow(){ PlaySource(TypeSource.Window);}
    private void PlayGameOver(){ PlaySource(TypeSource.GameOver);}
    private void PlayTouch() {PlaySource(TypeSource.Touch);}
    private void PlayTouch(bool value){PlaySource(TypeSource.Touch);}
    private void PlayCrystal(){PlaySource(TypeSource.Cristal);}
    
    public void PlaySource(TypeSource type)
    { if (MuteAudio) return;
        switch (type)
        {
            case TypeSource.Window: _sourceWindow.Play();
                break;
            case TypeSource.Touch: _sourceTouch.Play();
                break;
            case TypeSource.Cristal: _sourceCrystal.Play();
                break;
            case TypeSource.GameOver: _sourceGameOver.Play();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
}
    

