﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollower : MonoBehaviour
{
    
#pragma warning disable 0649 
    
    public Transform Target; 
   
    [Space][Header("Vector")] 
    [SerializeField] private float _speedVector;
    [SerializeField] private float _vectorY = 10;
    [SerializeField] private float _vectorZ = 10;
    [SerializeField] private float _vectorX;  
                                      
#pragma warning restore 0649 

    private Vector3 _temp;
                                     
    public static CamFollower Instance; 

    private void Awake()
    {          
        Instance = this;
    }

    private void Start()
    {
        LevelManager.Instance.OnLevelLost += OnLevelLost;
    } 
    
   
    private void OnLevelLost()
    {
        SetStop();
    }
    

    private void FixedUpdate()   
    {                 
        if (!Target) return;
       
         Move();
    }

    void Move()
    {
        _temp = Target.position;
        _temp.y += _vectorY;
        _temp.z += _vectorZ;
        _temp.x += _vectorX ;   
                                  
        transform.position = _temp;
        transform.LookAt(Target.position); 
    }

    public void SetStop()
    {
        Target = null;  
    }

}


